package HellKitchen.Agents;

import HellKitchen.Agents.Protocol.CookOrderProtocol;
import HellKitchen.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.*;


public class OrderAgent extends Agent {

    public static final String ORDER_SERVICE = "ORDER_AGENT_SERVICE";
    public static final String ORDER_SERVICE_NAME = "ORDER_AGENT_SERVICE_NAME";

    private String order;
    private boolean orderAccepted = false;

    @Override
    protected void setup() {
        super.setup();
        registerService();

        Object[] args = getArguments();

        if (args == null || args.length < 1) {
            System.err.println("Order's content not defined");
            doDelete();
            return;
        }

        order = (String) args[0];

        createBehaviours();

        System.out.println(getLocalName() + " was setup!");
    }

    @Override
    protected void takeDown() {
        System.out.println(getLocalName() + " shut down!");
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        super.takeDown();
    }

    private void registerService() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(ORDER_SERVICE);
        sd.setName(ORDER_SERVICE_NAME);
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    private void createBehaviours() {
        addBehaviour(new SendOfferBehaviour());
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                if (orderAccepted)
                    doDelete();
            }
        });
    }

    class SendOfferBehaviour extends Behaviour {

        private final Random random = new Random(42);

        private final MessageTemplate template = MessageTemplate.and(
                MessageTemplate.MatchConversationId(CookOrderProtocol.CONVERSATION_ID),
                MessageTemplate.or(
                        MessageTemplate.MatchPerformative(CookOrderProtocol.ACCEPT_ORDER),
                        MessageTemplate.MatchPerformative(CookOrderProtocol.REJECT_ORDER)
                )
        );

        @Override
        public void action() {
            List<AID> cooks = Utils.findService(myAgent, CookAgent.COOK_SERVICE);
            if (!cooks.isEmpty()) {
                int index = random.nextInt(cooks.size());
                ACLMessage msg = new ACLMessage(CookOrderProtocol.OFFER_ORDER);
                msg.setConversationId(CookOrderProtocol.CONVERSATION_ID);
                msg.addReceiver(cooks.get(index));
                msg.setContent(order);
                send(msg);
//                System.out.println(getLocalName() + " sent request to " + cooks.get(index).getLocalName());
                waitForAnswer();
            }
        }

        @Override
        public boolean done() {
            return orderAccepted;
        }

        private void waitForAnswer() {
            ACLMessage answer = blockingReceive(template, 2000);
            if (answer != null && answer.getPerformative() == CookOrderProtocol.ACCEPT_ORDER) {
//                System.out.println(getLocalName() + " receive accept answer from " + answer.getSender().getLocalName());
                orderAccepted = true;
            }
        }
    }

    @Override
    public String toString() {
        return getLocalName();
    }
}
