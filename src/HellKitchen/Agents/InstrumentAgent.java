package HellKitchen.Agents;

import HellKitchen.Agents.Protocol.CookInstrumentProtocol;
import HellKitchen.Agents.Protocol.SchedulerProtocol;
import HellKitchen.Model.DayTime;
import HellKitchen.Model.Operation;
import HellKitchen.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class InstrumentAgent extends Agent {

    public static final String OVEN_SERVICE = "OVEN_SERVICE";
    public static final String CUTBOARD_SERVICE = "BOARD_SERVICE";
    public static final String MIXER_SERVICE = "MIXER_SERVICE";
    public static final String DISHWASHER_SERVICE = "WASHER_SERVICE";

    private enum State {
        Waiting,
        Scheduling
    }

    private State state = State.Waiting;

    private String instrumentName;

    private List<Operation> operations = new ArrayList<>();
    private Queue<ACLMessage> requests = new LinkedList<>();

    @Override
    protected void setup() {
        super.setup();

        Object[] args = getArguments();
        if (args == null || args.length < 1) {
            System.err.println("Instrument's name not defined!");
            doDelete();
            return;
        }

        instrumentName = (String) args[0];

        registerService();
        createBehaviours();

        System.out.println(this + " was setup");
    }

    @Override
    protected void takeDown() {
        System.out.println(this + " shut down");
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        super.takeDown();
    }

    private void registerService() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(instrumentName.toUpperCase() + "_SERVICE");
        sd.setName(instrumentName.toUpperCase() + "_SERVICE_NAME");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    private void createBehaviours() {
        addBehaviour(new SendFreeTimeBehaviour());
    }

    class SendFreeTimeBehaviour extends CyclicBehaviour {

        private final MessageTemplate template = MessageTemplate.or(
                MessageTemplate.MatchConversationId(CookInstrumentProtocol.CONVERSATION_ID),
                MessageTemplate.MatchConversationId(SchedulerProtocol.CONVERSATION_ID)
        );

        @Override
        public void action() {
            ACLMessage request = blockingReceive(template);

            switch (request.getPerformative()) {
                case CookInstrumentProtocol.REQUEST_FREE_TIME:
                    sendFreeTime(request);
                    break;
                case CookInstrumentProtocol.SEND_PREFERRED_TIME:
                    System.out.println(myAgent + " received preferred time from " + request.getSender().getLocalName());
                    requests.add(request);
                    break;
                case SchedulerProtocol.SEND_OPERATION:
                    receiveOperation(request);
                    state = State.Waiting;
                    break;
            }

            if (requests.isEmpty())
                return;

            if (state == State.Waiting) {
                ACLMessage nextRequest = requests.remove();
                schedule(nextRequest);
                state = State.Scheduling;
            }
        }

    }

    private void sendFreeTime(ACLMessage request) {
        ACLMessage msg = request.createReply();
        msg.setPerformative(CookInstrumentProtocol.SEND_FREE_TIME);
        msg.setReplyWith(request.getReplyWith());
        if (operations.isEmpty()) {
            msg.setContent("");
        } else {
            DayTime freeTime = operations.get(operations.size() - 1).end;
            msg.setContent(freeTime.toString());
        }
        send(msg);
    }

    private void schedule(ACLMessage request) {
        String[] content = request.getContent().split(";");
        assert content.length > 0;

        String action = content[0];
        DayTime preferredTime = null;
        if (content.length > 1) {
            preferredTime = DayTime.fromString(content[1]);
        }

        DayTime startTime;
        if (operations.isEmpty()) {
            startTime = preferredTime;
        } else {
            DayTime freeTime = operations.get(operations.size() - 1).end;
            if (preferredTime == null) {
                startTime = freeTime;
            } else {
                startTime = preferredTime.isAfter(freeTime) ? preferredTime : freeTime;
            }
        }

        List<AID> scheduler = Utils.findService(this, SchedulerAgent.SCHEDULER_SERVICE);
        assert scheduler.size() > 0;

        ACLMessage msg = new ACLMessage(SchedulerProtocol.SEND_FINAL_TIME);
        msg.setConversationId(SchedulerProtocol.CONVERSATION_ID);
        msg.addReceiver(scheduler.get(0));

        String cook = request.getSender().getLocalName();
        String msgContent = cook + ";" + action;
        if (startTime != null) {
            msgContent += ";" + startTime.toString();
        }

        msg.setContent(msgContent);
        send(msg);
    }

    private void receiveOperation(ACLMessage request) {
        Operation operation = Operation.fromString(request.getContent());
        operations.add(operation);
        System.out.println(this + " received operation: " + operation);
    }

    @Override
    public String toString() {
        return getLocalName();
    }
}
