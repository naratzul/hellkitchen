package HellKitchen.Agents;

import jade.core.*;
import jade.wrapper.AgentContainer;
import jade.wrapper.StaleProxyException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class CreatorAgent extends Agent {

    @Override
    protected void setup() {
        super.setup();

        Object[] args = getArguments();
        if (args == null || args.length <= 0) {
            doDelete();
            return;
        };

        boolean isServer = Integer.valueOf((String) args[0]) == 1;
        String filename = (String) args[1];

        start(filename, isServer);
    }



    private void start(String filename, boolean isServer) {

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            LocalTime startTime = LocalTime.parse(reader.readLine());
            LocalTime endTime = LocalTime.parse(reader.readLine());
            int cookCount = Integer.valueOf(reader.readLine());
            int ovenCount = Integer.valueOf(reader.readLine());
            int cutBoardCount = Integer.valueOf(reader.readLine());
            int mixerCount = Integer.valueOf(reader.readLine());
            int tableCount = Integer.valueOf(reader.readLine());
            List<String> orders = readOrders(reader);

            if (isServer) {
                startServer(startTime, endTime, ovenCount, cutBoardCount, mixerCount, tableCount, cookCount);
            } else {
                startClient(cookCount, orders);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startServer(LocalTime startTime, LocalTime endTime, int ovenCount, int cutBoardCount, int mixerCount, int tableCount, int cookCount) {
        AgentContainer cc = getContainerController();

        try {

            for (int i = 1; i <= ovenCount; ++i) {
                Object[] args = new Object[]{ "OVEN", startTime};
                cc.createNewAgent("Oven" + i, InstrumentAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= cutBoardCount; ++i) {
                Object[] args = new Object[]{ "BOARD", startTime};
                cc.createNewAgent("Board" + i, InstrumentAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= mixerCount; ++i) {
                Object[] args = new Object[]{ "MIXER", startTime};
                cc.createNewAgent("Mixer" + i, InstrumentAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= tableCount; ++i) {
                Object[] args = new Object[]{ "TABLE", startTime};
                cc.createNewAgent("Table" + i, InstrumentAgent.class.getName(), args).start();
            }

            Object[] args = new Object[]{ startTime, endTime, cookCount };
            cc.createNewAgent("Scheduler", SchedulerAgent.class.getName(), args).start();
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }
    }

    private void startClient(int cookCount, List<String> orders) {
        AgentContainer cc = getContainerController();

        try {

            for (int i = 0; i < orders.size(); ++i) {
                Object[] args = new Object[]{ orders.get(i) };
                cc.createNewAgent("Order" + (i+1), OrderAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= cookCount; ++i) {
                cc.createNewAgent("Cook" + i, CookAgent.class.getName(), null).start();
            }

        } catch (StaleProxyException e) {
            e.printStackTrace();
        }
    }

    private List<String> readOrders(BufferedReader reader) throws IOException {
        List<String> orders = new ArrayList<>();

        String line = reader.readLine();
        while (line != null) {
            orders.add(line);
            line = reader.readLine();
        }

        return orders;
    }

}
