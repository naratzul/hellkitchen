package HellKitchen.Agents.Protocol;

import jade.lang.acl.ACLMessage;

/**
 * Created by naratzul on 07.04.19.
 */
public interface SchedulerProtocol {

    String CONVERSATION_ID = "SchedulerID";

    int SEND_FINAL_TIME = ACLMessage.PROPOSE;
    int SEND_OPERATION = ACLMessage.ACCEPT_PROPOSAL;

    int COOK_FINISHED = ACLMessage.INFORM;
}
