package HellKitchen.Agents.Protocol;

import jade.lang.acl.ACLMessage;

public interface CookOrderProtocol {

    String CONVERSATION_ID = "OrderCookID";

    int OFFER_ORDER = ACLMessage.PROPOSE;
    int ACCEPT_ORDER = ACLMessage.ACCEPT_PROPOSAL;
    int REJECT_ORDER = ACLMessage.REJECT_PROPOSAL;

}
