package HellKitchen.Agents.Protocol;

import jade.lang.acl.ACLMessage;

/**
 * Created by naratzul on 06.04.19.
 */
public interface CookInstrumentProtocol {

    String CONVERSATION_ID = "CookInstrumentID";

    int REQUEST_FREE_TIME = ACLMessage.REQUEST;
    int SEND_FREE_TIME = ACLMessage.CFP;
    int SEND_PREFERRED_TIME = ACLMessage.PROPOSE;
    int SEND_OPERATION = ACLMessage.ACCEPT_PROPOSAL;

}
