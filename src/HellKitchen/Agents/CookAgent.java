package HellKitchen.Agents;

import HellKitchen.Agents.Protocol.CookInstrumentProtocol;
import HellKitchen.Agents.Protocol.CookOrderProtocol;
import HellKitchen.Agents.Protocol.SchedulerProtocol;
import HellKitchen.Model.Action;
import HellKitchen.Model.DayTime;
import HellKitchen.Model.Operation;
import HellKitchen.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


public class CookAgent extends Agent {

    public static final String COOK_SERVICE = "COOK_AGENT_SERVICE";
    public static final String COOK_SERVICE_NAME = "COOK_AGENT_SERVICE_NAME";

    private enum State {
        Waiting,
        Scheduling
    }

    private State state = State.Waiting;

    private List<AID> activeOrders = new ArrayList<>();
    private List<String> completedOrders = new ArrayList<>();

    private List<Operation> operations = new ArrayList<>();

    @Override
    protected void setup() {
        super.setup();

        registerService();
        createBehaviours();

        System.out.println(getLocalName() + " was setup!");
    }

    @Override
    protected void takeDown() {
        System.out.println(getLocalName() + " handled orders " + completedOrders.toString());
        System.out.println(getLocalName() + " shut down!");
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        super.takeDown();
    }

    private void registerService() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(COOK_SERVICE);
        sd.setName(COOK_SERVICE_NAME);
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    private void createBehaviours() {
        addBehaviour(new ReceiveOrderBehaviour());
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                List<AID> orders = Utils.findService(myAgent, OrderAgent.ORDER_SERVICE);
                if (orders.isEmpty() && state == State.Waiting) {
                    informScheduler();
                    doDelete();
                }
            }
        });
    }

    class ReceiveOrderBehaviour extends Behaviour {

        private final MessageTemplate template = MessageTemplate.and(
                MessageTemplate.MatchConversationId(CookOrderProtocol.CONVERSATION_ID),
                MessageTemplate.MatchPerformative(CookOrderProtocol.OFFER_ORDER)
        );

        @Override
        public void action() {
            ACLMessage msg = receive(template);
            if (msg != null) {
//                System.out.println(getLocalName() + " receive offer from " + msg.getSender().getLocalName());
                switch (state) {
                    case Waiting:
                        sendAcceptAnswer(msg);
                        handleOrder(msg.getContent());
                        state = State.Scheduling;
                        break;
                    case Scheduling:
                        sendRejectAnswer(msg);
                        break;
                }
            }

            activeOrders = Utils.findService(myAgent, OrderAgent.ORDER_SERVICE);
        }

        @Override
        public boolean done() {
            return activeOrders.isEmpty();
        }
    }

    private void sendAcceptAnswer(ACLMessage requestMessage) {
        ACLMessage msg = requestMessage.createReply();
        msg.setPerformative(CookOrderProtocol.ACCEPT_ORDER);
        send(msg);
//        System.out.println(getLocalName() + " sent accept answer to " + requestMessage.getSender().getLocalName());
    }

    private void sendRejectAnswer(ACLMessage requestMessage) {
        ACLMessage msg = requestMessage.createReply();
        msg.setPerformative(CookOrderProtocol.REJECT_ORDER);
        send(msg);
//        System.out.println(getLocalName() + " sent reject answer to " + requestMessage.getSender().getLocalName());
    }

    private void handleOrder(String orderContent) {
        List<Action> actions = createActions(orderContent);
        SequentialBehaviour handleOrderBehaviour = new SequentialBehaviour(this) {
            @Override
            public int onEnd() {
                completedOrders.add(orderContent);
                state = State.Waiting;
                return super.onEnd();
            }
        };
        for (Action action : actions) {
            Behaviour beh = createHandleActionBehaviour(action);
            if (beh != null)
                handleOrderBehaviour.addSubBehaviour(beh);
        }
        addBehaviour(handleOrderBehaviour);
    }

    private List<Action> createActions(String orderContent) {
        return Action.actionsFromOrder(orderContent);
    }

    private Behaviour createHandleActionBehaviour(Action action) {
        List<AID> instruments = Utils.findService(this, getInstrumentType(action));
        if (instruments.isEmpty()) {
            System.out.println(this + " can't handle action " + action.type + ": there is no instruments");
            return null;
        }

        return new HandleActionBehaviour(this, action, instruments) {
            @Override
            public int onEnd() {
                System.out.println(myAgent + " completed action: " + action);
                return super.onEnd();
            }
        };
    }

    private String getInstrumentType(Action action) {
        switch (action.type) {
            case Cut:
                return InstrumentAgent.CUTBOARD_SERVICE;
            case BakeTart:
                return InstrumentAgent.OVEN_SERVICE;
            case BakeCupcake:
                return InstrumentAgent.OVEN_SERVICE;
            case WhipCream:
                return InstrumentAgent.MIXER_SERVICE;
            case Wash:
                return InstrumentAgent.DISHWASHER_SERVICE;
        }

        return null;
    }

    class HandleActionBehaviour extends SequentialBehaviour {

        HandleActionBehaviour(Agent agent, Action action, List<AID> instruments) {
            super(agent);

            String replyWith = myAgent.getLocalName() + System.currentTimeMillis();
            addSubBehaviour(new OneShotBehaviour() {
                @Override
                public void action() {
                    requestFreeTime(replyWith, instruments);
                }
            });

            ReceiveFreeTimeBehaviour beh = new ReceiveFreeTimeBehaviour(replyWith, instruments.size());
            beh.setDataStore(getDataStore());
            addSubBehaviour(beh);

            SendPreferredTimeBehaviour beh2 = new SendPreferredTimeBehaviour(action);
            beh2.setDataStore(getDataStore());
            addSubBehaviour(beh2);

            addSubBehaviour(new ReceiveOperationBehaviour());
        }
    }

    private void requestFreeTime(String replyWith, List<AID> instruments) {
        ACLMessage request = new ACLMessage(CookInstrumentProtocol.REQUEST_FREE_TIME);
        request.setConversationId(CookInstrumentProtocol.CONVERSATION_ID);
        request.setReplyWith(replyWith);
        for (AID instrument : instruments)
            request.addReceiver(instrument);
        send(request);
    }

    class ReceiveFreeTimeBehaviour extends Behaviour {

        static final String BEST_TIME = "BEST_TIME";
        static final String BEST_TIME_INSTRUMENT = "BEST_TIME_INSTRUMENT";

        private MessageTemplate template;

        private int instrumentsCount;
        private int receivedFreeTimeCount = 0;

        private AID bestInstrument;
        private DayTime bestTime = new DayTime(Integer.MAX_VALUE, LocalTime.MIDNIGHT);
        private DayTime preferredTime;
        private boolean foundBest = false;

        ReceiveFreeTimeBehaviour(String replyWith, int instrumentsCount) {
            super();
            this.instrumentsCount = instrumentsCount;
            template = MessageTemplate.and(
                    MessageTemplate.MatchConversationId(CookInstrumentProtocol.CONVERSATION_ID),
                    MessageTemplate.and(
                            MessageTemplate.MatchPerformative(CookInstrumentProtocol.SEND_FREE_TIME),
                            MessageTemplate.MatchReplyWith(replyWith)
                    )
            );
        }

        @Override
        public void onStart() {
            super.onStart();

            if (operations.isEmpty()) {
                preferredTime = null;
            } else {
                preferredTime = operations.get(operations.size() - 1).end;
//                bestTime = preferredTime;
            }

            System.out.println(myAgent + " started ReceiveFreeTimeBehaviour");
        }

        @Override
        public void action() {
            ACLMessage msg = receive(template);
            if (msg == null) return;

            String content = msg.getContent();
            AID instrument = msg.getSender();

            if (preferredTime != null && content.isEmpty()) {
                bestTime = preferredTime;
                bestInstrument = instrument;
                foundBest = true;
                return;
            }

            if (preferredTime == null && content.isEmpty()) {
                bestTime = null;
                bestInstrument = instrument;
                foundBest = true;
                return;
            }


            DayTime freeTime = DayTime.fromString(content);
            if (preferredTime == null) {
                if (freeTime.isEqualOrBefore(bestTime)) {
                    bestTime = freeTime;
                    bestInstrument = instrument;
                }
            } else {
                if (freeTime.isEqualOrBefore(preferredTime)) {
                    bestTime = preferredTime;
                    bestInstrument = instrument;
                    foundBest = true;
                    return;
                } else if (freeTime.isEqualOrAfter(preferredTime) && freeTime.isEqualOrBefore(bestTime)) {
                    bestTime = freeTime;
                    bestInstrument = instrument;
                }
            }

            receivedFreeTimeCount++;
        }

        @Override
        public boolean done() {
            return receivedFreeTimeCount >= instrumentsCount || foundBest;
        }

        @Override
        public int onEnd() {
            System.out.println(myAgent + " ended ReceiveFreeTimeBehaviour");
            getDataStore().put(BEST_TIME, bestTime);
            getDataStore().put(BEST_TIME_INSTRUMENT, bestInstrument);
            return super.onEnd();
        }
    }

    class SendPreferredTimeBehaviour extends OneShotBehaviour {

        private Action action;
        private AID instrument;
        private DayTime time;

        SendPreferredTimeBehaviour(Action action) {
            super();
            this.action = action;
        }

        @Override
        public void onStart() {
            super.onStart();
            instrument = (AID) getDataStore().get(ReceiveFreeTimeBehaviour.BEST_TIME_INSTRUMENT);
            time = (DayTime) getDataStore().get(ReceiveFreeTimeBehaviour.BEST_TIME);
        }

        @Override
        public void action() {
            assert instrument != null;

            ACLMessage msg = new ACLMessage(CookInstrumentProtocol.SEND_PREFERRED_TIME);
            msg.setConversationId(CookInstrumentProtocol.CONVERSATION_ID);
            msg.addReceiver(instrument);

            String content = action.toString();
            if (time != null) {
                content += ";" + time.toString();
            }

            msg.setContent(content);
            send(msg);

            System.out.println(myAgent + " sent preferred time " + time + " to " + instrument.getLocalName());
        }
    }

    class ReceiveOperationBehaviour extends OneShotBehaviour {

        private final MessageTemplate template = MessageTemplate.and(
                MessageTemplate.MatchConversationId(SchedulerProtocol.CONVERSATION_ID),
                MessageTemplate.MatchPerformative(SchedulerProtocol.SEND_OPERATION)
        );

        @Override
        public void onStart() {
            super.onStart();
            System.out.println(myAgent + " started ReceiveOperationBehaviour");
        }

        @Override
        public void action() {
            ACLMessage msg = blockingReceive(template);
            if (msg == null) return;

            Operation operation = Operation.fromString(msg.getContent());
            System.out.println(myAgent + " receive operation " + operation);
            operations.add(operation);
        }

        @Override
        public int onEnd() {
            System.out.println(myAgent + " ended ReceiveOperationBehaviour");
            return super.onEnd();
        }
    }

    private void informScheduler() {
        ACLMessage msg = new ACLMessage(SchedulerProtocol.COOK_FINISHED);
        msg.setConversationId(SchedulerProtocol.CONVERSATION_ID);
        List<AID> schedulers = Utils.findService(this, SchedulerAgent.SCHEDULER_SERVICE);
        for (AID sc : schedulers)
            msg.addReceiver(sc);
        send(msg);
    }

    @Override
    public String toString() {
        return getLocalName();
    }
}
