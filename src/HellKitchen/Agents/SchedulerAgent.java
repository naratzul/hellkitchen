package HellKitchen.Agents;

import HellKitchen.Agents.Protocol.SchedulerProtocol;
import HellKitchen.Model.Action;
import HellKitchen.Model.DayTime;
import HellKitchen.Model.Operation;
import HellKitchen.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by naratzul on 06.04.19.
 */
public class SchedulerAgent extends Agent {

    public static final String SCHEDULER_SERVICE = "SCHEDULER_SERVICE";
    public static final String SCHEDULER_SERVICE_NAME = "SCHEDULER_SERVICE_NAME";

    private List<Operation> operations = new ArrayList<>();

    private LocalTime workDayStart;
    private LocalTime workDayEnd;

    private int cookCount = 0;

    @Override
    protected void setup() {
        super.setup();

        Object[] args = getArguments();
        if (args == null || args.length < 2) {
            System.err.println("Scheduler's start time and end time not defined");
            doDelete();
            return;
        }

        workDayStart = (LocalTime) args[0];
        workDayEnd = (LocalTime) args[1];
        cookCount = (int) args[2];

        registerService();
        createBehaviours();
    }

    @Override
    protected void takeDown() {
        System.out.println(this + " shut down!");
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        super.takeDown();
    }

    private void registerService() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(SCHEDULER_SERVICE);
        sd.setName(SCHEDULER_SERVICE_NAME);
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    private void createBehaviours() {
        addBehaviour(new ReceiveOperationToSchedule());
        List<AID> cooks = Utils.findService(this, CookAgent.COOK_SERVICE);
        addBehaviour(new WaitingCooksFinishedBehaviour(cookCount));
    }

    class ReceiveOperationToSchedule extends CyclicBehaviour {

        private MessageTemplate template = MessageTemplate.and(
                MessageTemplate.MatchConversationId(SchedulerProtocol.CONVERSATION_ID),
                MessageTemplate.MatchPerformative(SchedulerProtocol.SEND_FINAL_TIME)
        );

        @Override
        public void action() {
            ACLMessage msg = receive(template);
            if (msg == null) return;

            AID instrument = msg.getSender();
            String[] content = msg.getContent().split(";");
            assert content.length >= 2; // cook + action + (optional) time
            String cookName = content[0];
            Action action = Action.fromString(content[1]);
            DayTime startTime;
            if (content.length >= 3) {
                startTime = DayTime.fromString(content[2]);
            } else {
                startTime = new DayTime(1, workDayStart);
            }

            Operation operation = createOperation(action, cookName, instrument.getLocalName(), startTime);
            AID cook = findCook(cookName);
            assert cook != null;
            sendOperation(operation, cook, instrument);
            operations.add(operation);
            System.out.println(myAgent + " scheduled operation: " + operation);
        }

    }

    private Operation createOperation(Action action, String cook, String instrument, DayTime start) {
        LocalTime endTime = start.time.plus(action.getDuration(), ChronoUnit.MINUTES);
        if (endTime.isAfter(workDayEnd)) {
            int day = start.day + 1;
            start = new DayTime(day, workDayStart);
            endTime = start.time.plus(action.getDuration(), ChronoUnit.MINUTES);
        }

        DayTime end = new DayTime(start.day, endTime);

        return new Operation(action, cook, instrument, start, end);
    }

    private AID findCook(String cookName) {
        List<AID> cooks = Utils.findService(this, CookAgent.COOK_SERVICE);
        AID cook = null;
        for (AID c : cooks) {
            if (c.getLocalName().equals(cookName)) {
                cook = c;
                break;
            }
        }
        return cook;
    }

    private void sendOperation(Operation operation, AID cook, AID instrument) {
        ACLMessage msg = new ACLMessage(SchedulerProtocol.SEND_OPERATION);
        msg.setConversationId(SchedulerProtocol.CONVERSATION_ID);
        msg.addReceiver(cook);
        msg.addReceiver(instrument);
        msg.setContent(operation.toString());
        send(msg);
    }

    class WaitingCooksFinishedBehaviour extends Behaviour {

        private final MessageTemplate template = MessageTemplate.and(
                MessageTemplate.MatchConversationId(SchedulerProtocol.CONVERSATION_ID),
                MessageTemplate.MatchPerformative(SchedulerProtocol.COOK_FINISHED)
        );

        private int cookCount;
        private int cookFinished = 0;

        WaitingCooksFinishedBehaviour(int cookCount) {
            this.cookCount = cookCount;
        }

        @Override
        public void action() {
            ACLMessage msg = receive(template);
            if (msg == null) return;

            cookFinished++;
        }

        @Override
        public boolean done() {
            return cookFinished >= cookCount;
        }

        @Override
        public int onEnd() {
            printSchedule();
            doDelete();
            return super.onEnd();
        }
    }

    private void printSchedule() {

        Map<Integer, List<Operation>> operationsPerDay = operations.stream()
                        .sorted(Comparator.comparing(o -> o.start))
                        .collect(Collectors.groupingBy(Operation::getDay,
                                        Collectors.toList()));


        try (FileWriter fr = new FileWriter("out.txt")) {
            for (Map.Entry<Integer, List<Operation>> entry : operationsPerDay.entrySet()) {
                int day = entry.getKey();
                fr.write("Day " + day);
                fr.write("\n");
                for (Operation operation : entry.getValue()) {
                    fr.write(operation.toPrettyString());
                    fr.write("\n");
                }
                fr.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return getLocalName();
    }
}
