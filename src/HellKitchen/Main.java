package HellKitchen;

import HellKitchen.Agents.CookAgent;
import HellKitchen.Agents.OrderAgent;
import HellKitchen.Agents.InstrumentAgent;
import HellKitchen.Agents.SchedulerAgent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("Укажите имя файла с заказами первым аргументом.");
            System.exit(1);
        }

        String filename = args[0];
        start(filename);
    }

    private static void start(String filename) {

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            LocalTime startTime = LocalTime.parse(reader.readLine());
            LocalTime endTime = LocalTime.parse(reader.readLine());
            int cookCount = Integer.valueOf(reader.readLine());
            int ovenCount = Integer.valueOf(reader.readLine());
            int cutBoardCount = Integer.valueOf(reader.readLine());
            int mixerCount = Integer.valueOf(reader.readLine());
            int dishWasherCount = Integer.valueOf(reader.readLine());
            List<String> orders = readOrders(reader);

            startKitchen(startTime, endTime, cookCount,
                    ovenCount, cutBoardCount, mixerCount,
                    dishWasherCount, orders);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> readOrders(BufferedReader reader) throws IOException {
        List<String> orders = new ArrayList<>();

        String line = reader.readLine();
        while (line != null) {
            orders.add(line);
            line = reader.readLine();
        }

        return orders;
    }

    private static void startKitchen(LocalTime startTime, LocalTime endTime,
                                     int cookCount, int ovenCount, int cutBoardCount,
                                     int mixerCount, int dishWasherCount, List<String> orders) {
        Runtime runtime = Runtime.instance();
        runtime.setCloseVM(true);
        runtime.invokeOnTermination(() -> System.out.println("Bye"));
        Profile profile = new ProfileImpl();
        profile.setParameter(Profile.GUI, Boolean.TRUE.toString());
        ContainerController cc = runtime.createMainContainer(profile);

        try {

            for (int i = 1; i <= ovenCount; ++i) {
                Object[] args = new Object[]{ "OVEN"};
                cc.createNewAgent("Oven" + i, InstrumentAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= cutBoardCount; ++i) {
                Object[] args = new Object[]{ "BOARD"};
                cc.createNewAgent("Board" + i, InstrumentAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= mixerCount; ++i) {
                Object[] args = new Object[]{ "MIXER"};
                cc.createNewAgent("Mixer" + i, InstrumentAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= dishWasherCount; ++i) {
                Object[] args = new Object[]{ "WASHER"};
                cc.createNewAgent("Washer" + i, InstrumentAgent.class.getName(), args).start();
            }

            for (int i = 0; i < orders.size(); ++i) {
                Object[] args = new Object[]{ orders.get(i) };
                cc.createNewAgent("Order" + (i+1), OrderAgent.class.getName(), args).start();
            }

            for (int i = 1; i <= cookCount; ++i) {
                cc.createNewAgent("Cook" + i, CookAgent.class.getName(), null).start();
            }

            Object[] args = new Object[]{ startTime, endTime };
            cc.createNewAgent("Scheduler", SchedulerAgent.class.getName(), args).start();
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }
    }
}
