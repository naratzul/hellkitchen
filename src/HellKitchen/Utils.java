package HellKitchen;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.*;


public class Utils {

    public static Set<String> splitUnique(String str, String delimiter) {
        Set<String> result = new HashSet<>();
        String[] parts = str.split(delimiter);
        Collections.addAll(result, parts);
        return result;
    }

    public static List<AID> findService(Agent agent, String serviceType) {
        List<AID> services = new ArrayList<>();

        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(serviceType);
        template.addServices(sd);

        try {
            DFAgentDescription[] result = DFService.search(agent, template);
            for (DFAgentDescription aResult : result)
                services.add(aResult.getName());
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        return services;
    }

}
