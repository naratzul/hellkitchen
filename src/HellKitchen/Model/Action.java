package HellKitchen.Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by naratzul on 04.04.19.
 */
public class Action {

    public enum Type {
        Cut(30),
        BakeTart(60),
        BakeCupcake(45),
        WhipCream(15),
        Wash(10);

        static Type fromString(String str) {
            if ("Cut".equalsIgnoreCase(str))
                return Cut;
            else if ("BakeTart".equalsIgnoreCase(str))
                return BakeTart;
            else if ("BakeCupcake".equalsIgnoreCase(str))
                return BakeCupcake;
            else if ("WhipCream".equalsIgnoreCase(str))
                return WhipCream;
            else if ("Wash".equalsIgnoreCase(str))
                return Wash;

            return null;
        }

        private int duration;

        Type(int duration) {
            this.duration = duration;
        }

        public String toPrettyString() {
            switch (this) {
                case Cut:
                    return "Cut";
                case BakeTart:
                    return "Bake TART";
                case BakeCupcake:
                    return "Bake CUPCAKE";
                case WhipCream:
                    return "Whip CREAM";
                case Wash:
                    return "Wash DISHES";
            }

            return "";
        }
    }

    public Type type;
    public String filling;

    public static List<Action> actionsFromOrder(String str) {
        String[] components = str.split("-");
        String dishType = components[0];

        List<Action> actions = new ArrayList<>();
        if (dishType.equalsIgnoreCase(Dish.TART)) {
            String filling = components[1];
            actions.addAll(Arrays.asList(new Action(Type.Cut, filling),
                    new Action(Type.BakeTart, filling), new Action(Type.Wash)));
        } else if (dishType.equalsIgnoreCase(Dish.CAKE)) {
            String filling = components[1];
            actions.addAll(Arrays.asList(new Action(Type.Cut, filling), new Action(Type.BakeCupcake),
                    new Action(Type.WhipCream), new Action(Type.Wash)));
        }

        return actions;
    }

    public static Action fromString(String str) {
        Scanner scanner = new Scanner(str);
        String type = scanner.next();
        String filling = null;
        if (scanner.hasNext()) {
            filling = scanner.next();
        }
        if (filling == null) {
            return new Action(Type.fromString(type));
        } else {
            return new Action(Type.fromString(type), filling);
        }
    }

    public Action(Type type) {
        this(type, null);
    }

    public Action(Type type, String filling) {
        this.type = type;
        this.filling = filling;
    }

    public int getDuration() {
        return type.duration;
    }

    public String toPrettyString() {
        String str = type.toPrettyString();
        if (filling != null) {
            str += " " + filling;
        }
        return str;
    }

    @Override
    public String toString() {
        String str = type.name();
        if (filling != null) {
            str += " " + filling;
        }
        return str;
    }
}
