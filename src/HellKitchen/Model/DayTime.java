package HellKitchen.Model;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Scanner;

/**
 * Created by naratzul on 06.04.19.
 */
public class DayTime implements Comparable<DayTime> {

    public int day;
    public LocalTime time;

    public static DayTime fromString(String str) {
        Scanner scanner = new Scanner(str);
        int day = Integer.valueOf(scanner.next());
        LocalTime time = LocalTime.parse(scanner.next());
        return new DayTime(day, time);
    }

    public DayTime(int day, LocalTime time) {
        this.day = day;
        this.time = time;
    }

    public boolean isAfter(DayTime dayTime) {
        return day > dayTime.day || (day == dayTime.day && time.isAfter(dayTime.time));
    }

    public boolean isEqualOrAfter(DayTime dayTime) {
        return isEqual(dayTime) || isAfter(dayTime);
    }

    public boolean isBefore(DayTime dayTime) {
        return day < dayTime.day || (day == dayTime.day && time.isBefore(dayTime.time));
    }

    public boolean isEqualOrBefore(DayTime dayTime) {
        return isEqual(dayTime) || isBefore(dayTime);
    }

    public boolean isEqual(DayTime dayTime) {
        return day == dayTime.day && time.equals(dayTime.time);
    }

    @Override
    public String toString() {
        return day + " " + time.toString();
    }

    @Override
    public int compareTo(DayTime o) {
        if (day < o.day)
            return -1;
        else if (day > o.day) {
            return 1;
        }

        return time.compareTo(o.time);
    }
}
