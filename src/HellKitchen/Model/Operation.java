package HellKitchen.Model;


/**
 * Created by naratzul on 06.04.19.
 */
public class Operation {

    public Action action;
    public String cook;
    public String instrument;
    public DayTime start;
    public DayTime end;

    public static Operation fromString(String str) {
        String[] components = str.split(";");
        Action action = Action.fromString(components[0]);
        String cook = components[1];
        String instrument = components[2];
        DayTime start = DayTime.fromString(components[3]);
        DayTime end = DayTime.fromString(components[4]);
        return new Operation(action, cook, instrument, start, end);
    }

    public Operation(Action action, String cook, String instrument, DayTime start, DayTime end) {
        this.action = action;
        this.cook = cook;
        this.instrument = instrument;
        this.start = start;
        this.end = end;
    }

    public int getDay() {
        return start.day;
    }

    public String toPrettyString() {
        return cook +
                "\t" +
                action.toPrettyString() +
                "\t" +
                instrument +
                "\t" +
                start.time.toString() +
                "\t" +
                end.time.toString();
    }

    @Override
    public String toString() {
        return action.toString() +
                ";" +
                cook +
                ";" +
                instrument +
                ";" +
                start.toString() +
                ";" +
                end.toString();
    }
}
